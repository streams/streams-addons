<?php

use Code\Extend\Hook;
use Code\Extend\Route;
use Code\Lib\Apps;
use Code\Lib\MessageFilter;

/**
 * Name: Not Safe For Work
 * Description: Collapse posts with inappropriate content
 * Version: 2.0
 * Author: Mike Macgirvin <https://macgirvin.com/profile/mike>
 * Maintainer: Mike Macgirvin <mike@macgirvin.com> 
 */

function nsfw_load() {
	Hook::register('prepare_body', 'addon/nsfw/nsfw.php', 'nsfw_prepare_body', 1, 10);
	Route::register('addon/nsfw/Mod_Nsfw.php','nsfw');
}


function nsfw_unload() {
	Hook::unregister('prepare_body', 'addon/nsfw/nsfw.php', 'nsfw_prepare_body', 1, 10);
	Route::unregister('addon/nsfw/Mod_Nsfw.php','nsfw');
}



function nsfw_prepare_body(&$b) {

	$words = null;

	if (local_channel() && Apps::addon_app_installed(local_channel(),'nsfw')) {
		$words = get_pconfig(local_channel(),'nsfw','words','nsfw,contentwarning,⚠️');
	}
	else {
		$words = 'nsfw,contentwarning,⚠️,sensitive';
	}

	if ($words) {
        $words = str_replace(',', "\n", $words);
	}

	$found = false;

	if (! get_safemode()) {
		$words = '';
	}

	if ($words) {
        MessageFilter::setLastMatch('');
        if ($matchingRule = MessageFilter::evaluate($b['item'],'',$words))  {
            return;
		}
        $matchingRule = MessageFilter::getLastMatch() ?? t('Filtered content');
	}

	$ob_hash = get_observer_hash();
	if ((! $ob_hash) 
		&& (intval($b['item']['author']['xchan_censored']) || intval($b['item']['author']['xchan_selfcensored']))) {
		$found = true;
		$orig_word = t('Possible adult content');
	}	

		$b['html'] = preg_replace('~<img[^>]*\K(?=src)~i','data-',$b['html']);

		if ($b['photo']) {
            $b['photo'] = preg_replace_callback("/\<img(.*?)src=\"(.*?)\"(.*?)\>/ism", "bb_colorbox", $b['photo']);
        }

        $b['html'] =  '<details><summary>' . sprintf( t('%s - view'),$matchingRule ) . '</summary>' . '<div>' . $b['html'] . '</div></details>';

}


