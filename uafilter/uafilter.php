<?php

use Code\Extend\Hook;
use Code\Lib\Config;

/**
 * Name: Uafilter
 * Description: Block profile access to your site members based on User-Agent.
 * Version: 1.0
 * Author: Macgirvin
 * Maintainer: none
 *
 */

/**
 * Example:
 * % util/addons install uafilter
 * % util/config uafilter.deny "gab,mastodon,fb_iab,instagram,facebookexternalua"
 *
 */



function uafilter_load() {
    Hook::register('channel_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('item_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('inbox_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('outbox_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('activity_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('conversation_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('replies_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('followers_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('following_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('lists_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('album_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('profile_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('search_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('feed_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('cloud_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('photos_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
    Hook::register('display_mod_init', 'addon/uafilter/uafilter.php', 'uafilter_main');
}

function uafilter_unload() {
    Hook::unregister_by_file('addon/uafilter/uafilter.php');
}

function uafilter_useragents()
{
    return explode(',', Config::Get('uafilter', 'deny', ''));
}

function uafilter_main($args)
{
    foreach (uafilter_useragents() as $agent) {
        $agent = trim($agent);
        if (!$agent) {
            continue;
        }
        if (str_contains(strtolower($_SERVER['HTTP_USER_AGENT']), $agent)) {
            logger('User-agent blocked: ' . argv(0) . ' ' . $_SERVER['HTTP_USER_AGENT'], LOGGER_DEBUG);
            http_status_exit(403, 'Permission denied.');
        }
    }
}
